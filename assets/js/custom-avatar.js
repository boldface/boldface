function renderMediaUploader( $ ) {
  'use strict';

  var file_frame, image_data;

  if( undefined !== file_frame ) {
    file_frame.open();
    return;
  }

  file_frame = wp.media.frames.file_frame = wp.media({
    frame: 'post',
    state: 'insert',
    multiple: false
  });

  file_frame.on( 'insert', function() {
    var json = file_frame.state().get('selection').first().toJSON();

    if( 0 > $.trim( json.url.length ) ) {
      return;
    }

    $("#custom_avatar").val(json.id);
    $(".user-profile-picture img").attr('src',json.url);
  });

  file_frame.open();
}

(function($) {
  $(document).ready(function() {
    $("#custom_avatar").click(function(e){
      e.preventDefault();
      renderMediaUploader( $ );
    });
    $(".user-profile-picture .description").css('display','none');
    $(".user-profile-picture").click(function(e){
      e.preventDefault();
      renderMediaUploader( $ );
    });
    $(".user-profile-picture").hover(function(e){
      $(".user-profile-picture").css('cursor','pointer');
    },function(e){
      $(".user-profile-picture").css('cursor','default');
    });
  });
})(jQuery);
