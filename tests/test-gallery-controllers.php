<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestGalleryControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->galleryViews = new \Boldface\Boldface\Views\gallery();
    $this->galleryModels = new \Boldface\Boldface\Models\gallery( $this->galleryViews );
    $this->galleryControllers = new \Boldface\Boldface\Controllers\gallery( $this->galleryModels );
    parent::__construct();
  }

  function testGalleryAdminInit() {
    $this->galleryControllers->admin_init();
    $this->assertSame( 10, has_filter( 'attachment_fields_to_edit', [ $this->galleryControllers->getModel(), 'attachment_fields_to_edit' ] ) );
    $this->assertSame( 10, has_filter( 'attachment_fields_to_save', [ $this->galleryControllers->getModel(), 'attachment_fields_to_save' ] ) );
  }

  function testGalleryWP() {
    $this->galleryControllers->wp();
    $this->assertSame( 10, has_filter( 'gallery_class', [ $this->galleryControllers->getView(), 'gallery_class' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Boldface\Models\lightbox\title', [ $this->galleryControllers->getModel(), 'lightbox_title' ] ) );
    $this->assertSame( 0, has_filter( 'Boldface\Boldface\Models\user\description', [ $this->galleryControllers->getModel(), 'author_bio' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Boldface\Views\user\sidebar\right', [ $this->galleryControllers->getModel(), 'aside' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\footer\text', [ $this->galleryControllers->getView(), 'powered_by' ] ) );
  }

  function testGalleryPriority() {
    $priority = $this->galleryControllers->getPriority();
    $this->assertSame( -1, $priority );
  }
}
