<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

$abspath = dirname( dirname( dirname( $_SERVER[ 'PWD' ] ) ) );
require_once $abspath . '/phpunit/includes/functions.php';

function _manually_load_environment() {
  \switch_theme( 'boldface' );
}
\tests_add_filter( 'muplugins_loaded', __NAMESPACE__ . '\_manually_load_environment' );

function _manually_load_modules( $modules ) {
  $modules[ 'gallery' ]  = '\Boldface\Boldface';
  $modules[ 'lightbox' ] = '\Boldface\Boldface';
  $modules[ 'user' ]     = '\Boldface\Boldface';
  return $modules;
}
\tests_add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\_manually_load_modules' );

require $abspath . '/phpunit/includes/bootstrap.php';
