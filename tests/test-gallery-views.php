<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestGalleryViews extends \WP_UnitTestCase {

  function __construct() {
    $this->galleryViews = new \Boldface\Boldface\Views\gallery();
    parent::__construct();
  }

  function testGalleryPoweredBy() {
    $gallery = $this->galleryViews->powered_by('');
    $this->assertSame( '<span class="text-muted">Boldface Design Cooperative</span>', $gallery );
  }

  function testGalleryClass() {
    $gallery = $this->galleryViews->gallery_class('');
    $this->assertSame( 'gallery d-flex flex-md-column flex-wrap justify-content-around justify-content-md-start', $gallery );
  }
}
