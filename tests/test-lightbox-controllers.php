<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestLightboxControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->lightboxViews = new \Boldface\Boldface\Views\lightbox();
    $this->lightboxModels = new \Boldface\Boldface\Models\lightbox( $this->lightboxViews );
    $this->lightboxControllers = new \Boldface\Boldface\Controllers\lightbox( $this->lightboxModels );
    parent::__construct();
  }


  function testLightboxWP() {
    $this->lightboxControllers->wp();
    $this->assertSame( 10, has_filter( 'wp_get_attachment_image_attributes', [ $this->lightboxControllers->getModel(), 'wp_get_attachment_image_attributes' ] ) );
    $this->assertSame( 10, has_filter( 'wp_get_attachment_link', [ $this->lightboxControllers->getModel(), 'wp_get_attachment_link' ] ) );
    $this->assertSame( 10, has_filter( 'wp_enqueue_scripts', [ $this->lightboxControllers->getModel(), 'enqueueScripts' ] ) );
  }

  function testLightboxPriority() {
    $priority = $this->lightboxControllers->getPriority();
    $this->assertSame( -1, $priority );
  }
}
