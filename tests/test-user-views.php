<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestUserViews extends \WP_UnitTestCase {

  function __construct() {
    $this->userViews = new \Boldface\Boldface\Views\user();
    parent::__construct();
  }

  function testUserHtml() {
    $user = $this->userViews->html();
    $this->expectOutputString( '<div class="container-fluid bg-light"><ul class="container user-list d-flex flex-wrap justify-content-start list-unstyled"></ul></div>' );
  }

  function testUserHtmlFilter() {
    \add_filter( 'Boldface\Boldface\Views\user\list', function() { return 'test'; } );
    $user = $this->userViews->html();
    $this->expectOutputString( '<div class="container-fluid bg-light"><ul class="container user-list d-flex flex-wrap justify-content-start list-unstyled">test</ul></div>' );
  }

  function testUserHtmlClassFilter() {
    \add_filter( 'Boldface\Boldface\Views\user\class', function() { return 'test'; } );
    $user = $this->userViews->html();
    $this->expectOutputString( '<div class="test"><ul class="container user-list d-flex flex-wrap justify-content-start list-unstyled"></ul></div>' );
  }

  function testUserHtmlListClassFilter() {
    \add_filter( 'Boldface\Boldface\Views\user\list\class', function() { return 'test'; } );
    $user = $this->userViews->html();
    $this->expectOutputString( '<div class="container-fluid bg-light"><ul class="test"></ul></div>' );
  }
}
