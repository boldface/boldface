<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestGalleryModels extends \WP_UnitTestCase {

  function __construct() {
    $this->galleryViews = new \Boldface\Boldface\Views\gallery();
    $this->galleryModels = new \Boldface\Boldface\Models\gallery( $this->galleryViews );
    parent::__construct();
  }

  function testGalleryAuthorBio() {
    $gallery = $this->galleryModels->author_bio('');
    $this->assertSame( '', $gallery );
  }

  function testGalleryAuthorBioGallery() {
    $gallery = $this->galleryModels->author_bio('[gallery]Gallery[/gallery]');
    $this->assertSame( '', $gallery );
  }
}
