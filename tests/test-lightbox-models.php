<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestLightboxModels extends \WP_UnitTestCase {

  function __construct() {
    $this->lightboxViews = new \Boldface\Boldface\Views\lightbox();
    $this->lightboxModels = new \Boldface\Boldface\Models\lightbox( $this->lightboxViews );
    parent::__construct();
  }

  function testLightboxWpGetAttachmentLink() {
    $lightbox = $this->lightboxModels->wp_get_attachment_link( '<a href="https://example.com/"><img src="img.jpg"></a>', 1, '150', false, false, '' );
    $this->assertSame( '<a href="" data-lightbox="gallery" data-title=""><img src="img.jpg"></a>', $lightbox );
  }

  function testLightboxWpGetAttachmentLinkValueFilter() {
    \add_filter( 'Boldface\Boldface\Models\lightbox\value', function() { return 'test'; } );
    $lightbox = $this->lightboxModels->wp_get_attachment_link( '<a href="https://example.com/"><img src="img.jpg"></a>', 1, '150', false, false, '' );
    $this->assertSame( '<a href="" data-lightbox="test" data-title=""><img src="img.jpg"></a>', $lightbox );
  }

  function testLightboxWpGetAttachmentLinkTitleFilter() {
    \add_filter( 'Boldface\Boldface\Models\lightbox\title', function() { return 'title'; } );
    $lightbox = $this->lightboxModels->wp_get_attachment_link( '<a href="https://example.com/"><img src="img.jpg"></a>', 1, '150', false, false, '' );
    $this->assertSame( '<a href="" data-lightbox="gallery" data-title="title"><img src="img.jpg"></a>', $lightbox );
  }

  function testLightboxWpGetAttachmentLinkAttributes() {
    $a = \wp_insert_post( [ 'post_content' => 'test' ] );
    $post = \get_post( $a );
    $lightbox = $this->lightboxModels->wp_get_attachment_image_attributes( [ 'class' => '' ], $post, '150' );
    $this->assertSame( 'img img-fluid', $lightbox[ 'class' ] );
  }

  function testLightboxEnqueueScripts() {
    $this->lightboxModels->enqueueScripts();
    $this->assertTrue( \wp_style_is( 'lightbox', 'enqueued' ) );
    $this->assertTrue( \wp_script_is( 'lightbox', 'enqueued' ) );
  }
}
