<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Tests;

class TestUserControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->userViews = new \Boldface\Boldface\Views\user();
    $this->userModels = new \Boldface\Boldface\Models\user( $this->userViews );
    $this->userControllers = new \Boldface\Boldface\Controllers\user( $this->userModels );
    parent::__construct();
  }

  function testUserWP() {
    $this->userControllers->wp();
    $this->assertSame( 10, has_filter( 'Boldface\Boldface\Views\user\list', [ $this->userControllers->getModel(), 'userList' ] ) );
  }

  function testUserInit() {
    $this->userControllers->init();
    $this->assertSame( 10, has_filter( 'get_avatar', [ $this->userControllers->getModel(), 'get_avatar' ] ) );
  }

  function testUserPriority() {
    $priority = $this->userControllers->getPriority();
    $this->assertSame( 10, $priority );
  }
}
