<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface;

/**
 * Class for updating the theme
 *
 * @since 1.0
 */
class updater extends \Boldface\Bootstrap\updater {
  protected $theme = 'boldface';
  protected $repository = 'boldface/boldface';
}
