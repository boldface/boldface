<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Views;

/**
 * Views for the lightbox
 *
 * @since 1.0
 */
class lightbox extends \Boldface\Bootstrap\Views\abstractViews {
}
