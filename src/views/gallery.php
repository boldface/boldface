<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface\Views;

/**
 * Views for the gallery
 *
 * @since 1.0
 */
class gallery extends \Boldface\Bootstrap\Views\abstractViews {

  /**
   * @var The gallery HTML
   *
   * @access public
   * @since  1.0
   */
  public $html = '';

  /**
   * Return the filtered powered by text
   *
   * @access public
   * @since  1.0
   *
   * @param string $css The powered by text
   *
   * @return string The powered by text
   */
  public function powered_by( string $content ) : string {
    return '<span class="text-muted">Boldface Design Cooperative</span>';
  }

  /**
   * Return the filtered gallery class
   *
   * @access public
   * @since  1.0
   *
   * @param string $css The unfiltered gallery class
   *
   * @return string The filtered gallery class
   */
  public function gallery_class( string $class ) : string {
    return 'gallery d-flex flex-md-column flex-wrap justify-content-around justify-content-md-start';
  }
}
