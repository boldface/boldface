<?php

/**
 * @package Boldface\Boldface
 */
declare( strict_types = 1 );
namespace Boldface\Boldface;

/**
 * Filter the controllers list.
 *
 * Add the default Bootstrap popovers, plug custom gallery, user, and lightbox.
 *
 * @since 1.0
 *
 * @param array $list List of controllers
 *
 * @return array The new list of controllers
 */
function controllers_list( array $list ) : array {
  $list[] = 'images';
  $list[] = 'openGraph';
  $list[] = 'popovers';
  $list[] = 'social';
  foreach( [ 'gallery', 'user', 'lightbox' ] as $item ) {
    require get_stylesheet_directory() . "/src/controllers/$item.php";
    require get_stylesheet_directory() . "/src/models/$item.php";
    require get_stylesheet_directory() . "/src/views/$item.php";
    $list[ $item ]= '\Boldface\Boldface';
  }
  return $list;
}
\add_filter( 'Boldface\Bootstrap\Controllers\modules', 'Boldface\Boldface\controllers_list' );

/**
 * Add updater controller to the admin_init hook
 *
 * @since 1.0
 */
function admin_init() {
  require __DIR__ . '/src/updater.php';
  $updater = new \Boldface\Boldface\updater();
  $updater->admin_init();
}
\add_action( 'admin_init', 'Boldface\Boldface\admin_init' );

/**
 * Use the wp_enqueue_scripts hook to enqueue style.
 *
 * @since 1.0
 */
function wp_enqueue_scripts() {
  \wp_enqueue_style( 'boldface', \get_stylesheet_directory_uri() . '/assets/css/style.css', [ 'booter' ] );
}
\add_action( 'wp_enqueue_scripts', 'Boldface\Boldface\wp_enqueue_scripts' );

/**
 * Add updater controller to the admin_init hook
 *
 * @since 1.0
 *
 * @param array $modules The array of modules loaded.
 */
function bootstrapInit( array $modules ) {
  \add_filter( 'Boldface\Boldface\Views\user\sidebar\left', function( $html ) use ( $modules ) {
    ob_start();
    $modules[ 'social' ]->getView()->html();
    $social = ob_get_clean();
    return $html . $social;
  }, 20 );
}
\add_action( 'Boldface\Bootstrap\init', 'Boldface\Boldface\bootstrapInit' );

/**
 * Filter the social media class.
 *
 * @since 1.0
 *
 * @param string $class The class used for social media links.
 * @param int    $count The number of social media links.
 *
 * @return string The filtered social media class.
 */
function socialClass( string $class, int $count ) : string {
  return $count <= 3 ? 'col-4' : 'col';
}
\add_filter( 'Boldface\Bootstrap\Views\social\wrapper\class', 'Boldface\Boldface\socialClass', 10, 2 );

/**
 * Add filter to bypass the Bootstrap REST API.
 *
 * @since 1.0
 */
\add_filter( 'Boldface\Bootstrap\REST', '__return_false' );
